<?php
/*
 * 実行コマンド
 * php getImageUrl.php screen_name [count](略可)
 * */
echo "結果はresult.txtに出力した\n\n";

require_once("twitteroauth-master/autoload.php");
use Abraham\TwitterOAuth\TwitterOAuth;

const CONSUMER_KEY = '';
const CONSUMER_SECRET = '';
$access_token = '';
$access_token_secret = '';

$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $access_token, $access_token_secret);

// 投稿件数[int]
$count = $argv[2]??100;
$acount = $argv[1]??'';

// 検索オプション
$tweets_params = ['screen_name' => $acount ,'count' => $count ,'exclude_replies' => false];

// 検索する
$statuses = $connection->get("statuses/user_timeline", $tweets_params);
if(!empty($statuses)){
    if(file_exists('result.txt'))unlink('result.txt');
    foreach ($statuses as $object){
        $expand_url = $object->extended_entities->media[0]->expanded_url."\t\t\t";
        file_put_contents('result.txt',$expand_url,FILE_APPEND | LOCK_EX);
        $display_url = $object->extended_entities->media[0]->display_url."\n";
        file_put_contents('result.txt',$display_url,FILE_APPEND | LOCK_EX);
    }
}